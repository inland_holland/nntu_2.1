#include <iostream>
#include <string>
#include <time.h>
using namespace std;

enum sex {M,F}; // пол - M-male ; F - female;
enum color {bluemerle,tricolour,sable=3}; // окрас: мраморный, трехцветный, соболиный

class Breeder;
class Dog
{ 
  friend class Breeder;
  public: //открыто
  sex sex_dog; // пол
  color color_dog; // окрас
  // методы класса
  void enter_sex(); // ввод пола
  void enter_color(); // ввод окраса
 
  Dog() // конструктор без параметров
  {   
    enter_sex();
    enter_color();
  }
  
  ~Dog(){}; // деструктор
	
   bool operator != (Dog s2) // переопределенный оператор != (если пол разный - true)
   {  
     if (sex_dog!=s2.sex_dog) 
       return true;
     else  
       return false;
   }
   void operator + (Dog s2) // переопределенный оператор +  (результат по окрасу)
   {
     int ind = color_dog + s2.color_dog;
     switch(ind)
     { 
       case (0): cout << endl << "50% bluemerle + 25% tricolour + 25 whitemerle" << endl << "be careful! whitemerle as konown as yearshal white is a dangerous genetic mutation" << endl; 
         break;
       case (1): cout << "50% tricolour + 50% blumerle" << endl; 
         break;
       case (2): cout << "100% tricolor" << endl; 
         break;
       case (3): cout << "50% sable + 25% tricolour + 25% sablemerle" << endl << "this combination is prohibited by FCI" << endl; 
         break;
       case (4): cout << "50% sable + 50% tricolour" << endl; 
         break;
       default: cout <<  "75% sable + 25% tricolour" << endl;
     }
   }
};

class Breeder
{
  friend class Dog;
  public:  
  string surname_Breeder; // фамилия заводчика
  string name; //кличка животного

  void enter_name(); // ввод клички
  void enter_breeder_surname(); // ввод фамилии
  Breeder()
  {
    enter_breeder_surname();
    enter_name();
  }
  ~Breeder(){}; // деструктор;
};

void Breeder::enter_name()
    {  
       cout << "enter the dog's name. if it contains more than 1 word, write in format Aa_Bb_Cc" << endl;
       cin >> name;  
    }

  void Breeder::enter_breeder_surname()
    {  
       cout << "enter the breeder's surname and first letter of name in format Aaa_B" << endl;
       cin >> surname_Breeder;  
    }

  void Dog::enter_color()
    {  
       int ss;
       cout << "enter the dog's colour: 2 - sable, 1 - tricolour, 0 - bluemerle" << endl;
       cin  >> ss;
       switch (ss)
       { 
          case (0): color_dog = bluemerle;
            break;
	  case (1): color_dog = tricolour;
            break;
          case (2): color_dog = sable;
       }
    }

  void Dog::enter_sex()
    {  
       char ss;
       cout << "enter the dog's sex: M/F" << endl;
       cin >> ss;
       if (ss=='M') 
         sex_dog=M;
       else 
         sex_dog=F;  
    }

 ostream &operator << (ostream &os,const Dog &s2) // переопределенный оператор вывода для Dog
    {
       switch(s2.color_dog)
       {
         case (bluemerle): os << "bluemerle" << endl;
           break;
	 case (tricolour): os << "tricolour" << endl;
           break;
         case (sable): os << "sable" << endl;
       }
       if (s2.sex_dog==M) 
       os << "male" << endl;
       else 
       os << "female" << endl;   
       return os;
    }

  ostream &operator << (ostream &os,const Breeder &s2)// переопределенный оператор вывода для Breeder
    {
       os << endl << "Breeder:" << s2.surname_Breeder << endl;
       os << endl << "Name:" << s2.name << endl;
       return os;
    }

  int main()
{
   Breeder ff; Dog aa; //вводим заводчика и собаку
   Breeder gg; Dog dd; //вводим заводчика и собаку
      if (aa!=dd)// проверяем по полу
       { 
         cout << ff; cout << aa; // если можно по полу, выводим данные по заводчику и собаке
	 cout << gg; cout << dd; // если можно по полу, выводим данные по заводчику и собаке
	 aa + dd; // результат по окрасу
       }
      else 
         cout << "error! choose male dog and female dog"; // если нельзя по полу
   return 0;
} 
