#include <iostream>

using namespace std;
 
struct animal // структура animal
{
  char name[100], breed[100];
  
  double weight, coeffmain, coefftype; // вес, коэффициент основной (по весу), коэффициент по типу  

  double entweight() // ввод веса в кг
    {  
      cout << "Enter the weight of dog, kg" << endl;
      cin >> weight;
      return weight;
    };
  
  double coeffmaincount() // расчет основного коэффициента (по весу)
    {
      coeffmain=weight*0.03;
      return coeffmain;
    };
  int agegroup; // возрастная группа
  union 
  { 
      int activity; // активность
      int growthtemp; // темп развития
      int consttype; // тип конституции
  }; 
};
 
int main()
{ 

  animal dog;
  cout << "Enter the dog's name" << endl;
  cin >> dog.name;
  cout << "Enter the dog's breed" << endl;
  cin >> dog.breed;
   
  double amtfood;
  dog.entweight();
 
  cout << "Enter the agegroup of your dog: 0 - puppy (<9 months), 1 - junior (9-18 months), 2 - adult (>18 months)" << endl;
  cin >> dog.agegroup;
  
  switch (dog.agegroup)
  {
    case 0:
    cout << "Enter the level of the puppy's activity: 0 if it is hyperactive, 1 if it is normally active, 2 if it is not active" << endl; // уровень активности: гиперактивный, нормальная активность, неактивный
    cin >> dog.activity;
      if (dog.activity==0) dog.coefftype=1.25;
      else if (dog.activity==1) dog.coefftype=1;
      else if (dog.activity==2) dog.coefftype=0.75;
    break;
    case 1:
    cout << "Enter the level of the junior's growth temp: 0 if it grows slowly, 1 if it is grows normally, 2 if it grows too fast" << endl; // темп развития: медленно развивается, нормально развивается, слишком быстро развивается
    cin >> dog.growthtemp;
      if (dog.growthtemp==0) dog.coefftype=1.25;
      else if (dog.growthtemp==1) dog.coefftype=1;
      else if (dog.growthtemp==2) dog.coefftype=0.75;
    break;
    case 2:
    cout << "Enter the type of the dog's constitution: 0 if it is slender, 1 if it is muscular, 2 if it is massive" << endl; // тип конституции: изящный (сухой), мускулистый (крепкий), массивный (тяжелый)
    cin >> dog.consttype;
      if (dog.consttype==0) dog.coefftype=1.25;
      else if (dog.consttype==1) dog.coefftype=1;
      else if (dog.consttype==2) dog.coefftype=0.75;
    break;
    default: cout << "Error!" << endl;
  };
  
  dog.coeffmaincount();
  amtfood=dog.coeffmain*dog.coefftype;
  cout << amtfood << " kg - amount of food your dog needs" << endl;
  return 0;
}

