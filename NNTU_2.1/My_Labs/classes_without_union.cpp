#include <iostream>
#include <string>

using namespace std;
enum sex {M,F,unknownsex}; // пол: кобель, сука, неизвестен
enum colour {sable,tricolour,bluemerle=3,unknowncolour=5}; // окрас: соболиный (рыжий), трехцветный, 
                                                   // блю-мерль (мраморный), не определен 
enum hairtype {standart,longhaired,unknownhairtype}; // тип шерсти: стандартный, длинношерстный, не определен 

class Sheepdogs // класс Овчарки
{
  public:
   sex sex_dog; // пол
   int breed; // порода
   hairtype hairtype_dog; // тип шерсти
   colour colour_dog; // окрас
   void enter_breed(); // ввод породы
   sex enter_sex(); // ввод породы
   void puppies(colour,colour); // окрас щенков
   void puppies(hairtype,hairtype); // тип шерсти щенков
   
   Sheepdogs() // конструктор без параметров
   {
     this->breed=2;
     this->sex_dog=unknownsex;
     this->colour_dog=unknowncolour;
     this->hairtype_dog=unknownhairtype;
   };

   Sheepdogs(int Breed,sex Sex_dog) // конструктор с 2 параметрами: порода, пол
   {
     this->breed=Breed;
     this->sex_dog=Sex_dog;
     this->colour_dog=unknowncolour;
     this->hairtype_dog=unknownhairtype;
   }; 
   
  ~Sheepdogs(){}; // деструктор
};

void Sheepdogs::enter_breed()
{
  int ct, ht;
  cout << "enter your dog's breed: " << endl << "0 if it is german sheepdog (GS), 1 if it is scotland sheepdog (collie)" << endl;
  cin >> breed;
  switch (breed)
  {
    case 0:
    cout << "enter 0 if your dog has standart type of hair, " << endl << "enter 1 if your dog is long-haired" << endl;
    cin >> ht;
      if (ht==0)
       hairtype_dog=standart;
      else if (ht==1)
       hairtype_dog=longhaired;
      else
       cout << "error!" << endl;
    break;
    
    case 1:
    cout << "enter the colour of your dog: " << endl << "0 if it is sable, 1 if it is tricolour, 2 if it is blue-merle" << endl;
    cin >> ct;
      if (ct==0)
       colour_dog=sable;
      else if (ct==1)
       colour_dog=tricolour;
      else if (ct==2)
       colour_dog=bluemerle;
      else 
       cout << "error!" << endl;
    break;
    
    default:
    cout << "error!" << endl;
  }
}
    
sex Sheepdogs::enter_sex()
{
  char s;
  cout << "enter your dog's sex: M if it is male, F if it is female" << endl;
  cin >> s;
  switch (s)
   {
     case 'M':
      sex_dog=M;
      break;
     case 'F':
      sex_dog=F;
      break;
     default:
      sex_dog=unknownsex;
   }
} 

void Sheepdogs::puppies(hairtype a,hairtype b)
{
  int comb=a+b;
  switch(comb)
  {
    case 0: 
     cout << "standart + standart = 100% standart" << endl; 
     break;
    case 1:
     cout << "standart + long-haired = 75% standart + 25% long-haired" << endl; 
     break;
    case 2: 
     cout << "long-haired + long-haired = 50% standart + 50% long-haired" << endl;
     break;
    default: 
     cout << "error!" << endl;
   }
}

void Sheepdogs::puppies(colour a,colour b)
{
  int comb=a+b;
  switch(comb)
  {
    case 0:
     cout << "sable + sable = 100% sable" << endl;
     break;
    case 1:
     cout << "sable + tricolour = 50% sable + 50% tricolour" << endl;
     break;
    case 2:
     cout << "tricolour + tricolour = 100% tricolour" << endl;
     break;
    case 3: 
     cout << "sable + blue-merle = 50% sable + 25% tricolour + 25% sable-merle. " << endl << "this combination is prohibited by FCI!!!" << endl;
     break;
    case 4:
     cout << "tricolour + blue-merle = 50% tricolour + 50% blue-merle" << endl;
     break;
    case 6:
     cout << "blue-merle + blue-merle = 50% blue-merle + 25% tricolour + 25% white-merle. " << endl << "be careful! white-merle as known as lethal white is a dangerous genetic mutation" << endl;
     break;
    default:
     cout << "error!" << endl;
  }
} 

int main()
{
  Sheepdogs dog1, dog2;
  cout << "First dog: " << endl;
  dog1.enter_breed();
  dog1.enter_sex();
  cout << "Second dog: " << endl;
  dog2.enter_breed();
  dog2.enter_sex();
  if ((dog1.breed!=dog2.breed) || (dog1.sex_dog==dog2.sex_dog))
   cout << "error!" << endl;
  else 
   {
     cout << "First dog: " << endl;
     cout << "Second dog: " << endl;
     if (dog1.breed==0)
      dog1.Sheepdogs::puppies(dog1.hairtype_dog,dog2.hairtype_dog);
     else if (dog1.breed==1)
      dog1.Sheepdogs::puppies(dog1.colour_dog,dog2.colour_dog);
   }
  return 0;
}
      
   
  
      
  

   
